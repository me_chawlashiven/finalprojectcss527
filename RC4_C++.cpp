#include "cryptopp/arc4.h"
using CryptoPP::ARC4;

#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include <iostream>
using std::cout;
using std::endl;

using std::string;

int main(int argc, char* argv[]) {

	//~Key and IV Generation/Initialization======================================
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	string key = "key";
	string msg = argv[1];//"hello";

	//Set state
	ARC4 arc4((byte*)key.data(), key.size());
	// Encrypt
	arc4.ProcessData((byte*)msg.data(), (byte*)msg.data(), msg.size());
	cout << msg << endl;

	// Reset state
	arc4.SetKey((byte*)key.data(), key.size());
	// Decrypt
	arc4.ProcessData((byte*)msg.data(), (byte*)msg.data(), msg.size());

	cout << msg << endl;
	
	return 0;
}
