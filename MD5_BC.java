//package BouncyCastleImplementation;

import java.security.Security;
import java.security.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.*;
import org.bouncycastle.crypto.digests.*;

public class MD5_BC {
    public static void main(String[] args) throws Exception {
        //add the security provider
        //not required if you have Install the library
        //by Configuring the Java Runtime
        Security.addProvider(new BouncyCastleProvider());

        //this is the input;
        byte input[] = args[0].getBytes();

        //update the input of MD5
        MD5Digest md5 = new MD5Digest();
        md5.update(input, 0, input.length);

        //get the output/ digest size and hash it
        byte[] digest = new byte[md5.getDigestSize()];
        md5.doFinal(digest, 0);

        //show the input and output
        System.out.println("Input (hex): " +
            new String(Hex.encode(input)));
        System.out.println("Output (hex): " +
            new String(Hex.encode(digest)));
            
		// NEEDED if you are using a Java version without SHA-256    
		Security.addProvider(new BouncyCastleProvider());

		// then go as usual 
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		String text = args[0];
		md.update(text.getBytes("UTF-8")); // or UTF-16 if needed
		digest = md.digest();
    }
}
