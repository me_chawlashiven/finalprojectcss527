//package BouncyCastleImplementation;

import java.security.Security;
import java.security.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.*;
import org.bouncycastle.crypto.digests.*;

public class SHA256_BC {
    public static void main(String[] args) { 
		try
		{
		// NEEDED if you are using a Java version without SHA-256    
		Security.addProvider(new BouncyCastleProvider());

		// then go as usual 
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		String text = args[0];
		md.update(text.getBytes("UTF-8")); // or UTF-16 if needed
		byte[] digest = md.digest();
		 } 
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
    }
}
