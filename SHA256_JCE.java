import java.security.*;

class SHA256_JCE {
   public static void main(String[] args)
   {
      try
      {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			String text = args[0];

			md.update(text.getBytes("UTF-8")); // Change this to "UTF-16" if needed
			byte[] digest = md.digest();
			System.out.println(String.format("%064x", new java.math.BigInteger(1, digest)));
      }
      catch (Exception e)
      {
         System.out.println("Exception: "+e);
      }
   }
}
