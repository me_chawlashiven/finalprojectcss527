#include <iostream>

#include <cryptopp/md5.h>
#include <cryptopp/integer.h>
#include <cryptopp/osrng.h>
#include "cryptopp/hex.h"
#include "cryptopp/filters.h"

int main(int argc, char* argv[])
{
	CryptoPP::MD5 hash;
	byte digest[ CryptoPP::MD5::DIGESTSIZE ];
	std::string message = argv[1];//"abcdefghijklmnopqrstuvwxyz";

	hash.CalculateDigest( digest, (byte*) message.c_str(), message.length() );

	CryptoPP::HexEncoder encoder;
	std::string output;
	encoder.Attach( new CryptoPP::StringSink( output ) );
	encoder.Put( digest, sizeof(digest) );
	encoder.MessageEnd();

	std::cout << output << std::endl;
}
